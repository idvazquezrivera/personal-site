<style>
<?php include 'media/style.css'; ?>
</style>
<?php

function __autoload ($class)
{
  $paths = array(
    $_SERVER["DOCUMENT_ROOT"].'/git/AriSql/classes/',
    $_SERVER["DOCUMENT_ROOT"].'/git/tiniMVC/'
  );

  foreach($paths as $path)
  {

    if (file_exists($path.$class.'.php'))
    {
      require_once $path.$class.'.php';
      break;
    }
    else {
          debug($path.$class.'.php');
    }
  }
}

function debug($var)
{
  echo '<pre class="debug">';
  $caller = debug_backtrace();
  $caller = array_shift($caller);
  echo '<b>file: </b>'.$caller['file'].'<br/>';
  echo '<b>line: </b>'.$caller['line'].'<br/>';
  print_r($var);
  echo '</pre>';
}
?>
